package com.blogging.application.service.impl;

import com.blogging.application.dto.CategoryDto;
import com.blogging.application.entity.Category;
import com.blogging.application.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CategoryServiceImplTest {

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    ModelMapper modelMapper;


    @Test
    public void createCategoryTest() {
        System.out.println("HEllo");
        Category category = new Category();
        category.setCategoryName("Rubix");
        category.setCategoryType("Game");
        category.setDescription("category");
        category.setPosts(new ArrayList<>());

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setCategoryName("Rubix");
        categoryDto.setCategoryType("Game");
        categoryDto.setDescription("category");
        category.setPosts(new ArrayList<>());
        when(this.modelMapper.map(categoryDto, Category.class)).thenReturn(category);
        when(categoryRepository.save(category)).thenReturn(category);
        when(this.modelMapper.map(category, CategoryDto.class)).thenReturn(categoryDto);

        CategoryDto createdCategory = categoryService.createCategory(categoryDto);
        assertEquals("Rubix", createdCategory.getCategoryName());
    }


    @Test
    public void testForJenkinsLearn(){
        assertEquals(true,true);
    }




}
