package com.blogging.application.controller;

import com.blogging.application.dto.CategoryDto;
import com.blogging.application.entity.ApiResponse;
import com.blogging.application.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {


    @Autowired
    CategoryService categoryService;


    @PostMapping("/create_category")
    public ResponseEntity<CategoryDto> createCategory(@Valid @RequestBody CategoryDto categoryDto) {
        CategoryDto savedCategory = this.categoryService.createCategory(categoryDto);
        return new ResponseEntity<>(savedCategory, HttpStatus.CREATED);

    }


    @GetMapping("/get_category/{categoryId}")
    public ResponseEntity<CategoryDto> getCategory(@PathVariable("categoryId") int categoryId) {
        CategoryDto categoryDto = this.categoryService.getCategory(categoryId);
        return new ResponseEntity<>(categoryDto, HttpStatus.OK);
    }


    @PutMapping("/update_category/{categoryId}")
    public ResponseEntity<CategoryDto> updateCategory(@Valid @RequestBody CategoryDto categoryDto, @PathVariable("categoryid") int categoryid) {
        CategoryDto updatedCategory = this.categoryService.updateCategory(categoryDto, categoryid);
        return new ResponseEntity<>(updatedCategory, HttpStatus.ACCEPTED);

    }

    @DeleteMapping("/delete_category/{categoryId}")
    public ResponseEntity<ApiResponse> deleteCategory(@PathVariable("categoryId") int categoryId) {
        this.categoryService.deleteCategory(categoryId);
        ApiResponse apiResponse = new ApiResponse("Deleted Successfully", true);
        return new ResponseEntity<>(apiResponse, HttpStatus.NO_CONTENT);
    }

    @GetMapping("/list")
    public ResponseEntity<List<CategoryDto>> listCategories() {
        List<CategoryDto> list = this.categoryService.listCategories();
        return new ResponseEntity<>(list, HttpStatus.OK);

    }
}
