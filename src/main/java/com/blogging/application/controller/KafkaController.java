package com.blogging.application.controller;

import com.blogging.application.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/kafka")
public class KafkaController {


    @Autowired
    private KafkaTemplate<String, Object> template;

    private String topic = "user";

    @GetMapping("/publish/{name}")
    public String publishMessage(@PathVariable String name) {
        template.send(topic, "Hi " + name + " Welcome to java techie");
        return "Data published";
    }

    public String publishJson(String topicName, String key, Object data) {
        template.send(topicName, key, data);
        return "User Data Published For Email:";
    }
}
