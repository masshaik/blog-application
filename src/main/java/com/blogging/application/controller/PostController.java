package com.blogging.application.controller;

import com.blogging.application.entity.ApiResponse;
import com.blogging.application.entity.Post;
import com.blogging.application.entity.PostDto;
import com.blogging.application.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("posts")
public class PostController {


    @Autowired
    PostService postService;


    @PostMapping("/create_post/user/{userId}/category/{categoryId}")
    public ResponseEntity<Post> createPost(@RequestBody PostDto postDto, @PathVariable("userId") int userid, @PathVariable("categoryId") int categoryId) {
        Post post = this.postService.createPost(postDto, userid, categoryId);
        return new ResponseEntity<>(post, HttpStatus.CREATED);

    }


    @GetMapping("/user/{userId}/posts")
    public ResponseEntity<List<PostDto>> getAllPostsByUser(@PathVariable("userId") int userId){
        List<PostDto> postDtos = this.postService.getAllPostsByUser(userId);
        return new ResponseEntity<>(postDtos,HttpStatus.OK);
    }

    @GetMapping("/category/{categoryId}/posts")
    public ResponseEntity<List<PostDto>> getAllPostsByCategory(@PathVariable("categoryId") int categoryId){
        List<PostDto> postDtos = this.postService.getAllPostsByCategory(categoryId);
        return new ResponseEntity<>(postDtos,HttpStatus.OK);
    }


    @GetMapping("/list_posts")
    public ResponseEntity<List<PostDto>> getAllPosts(){
        List<PostDto> postDtos = this.postService.getAllPost();
        return new ResponseEntity<>(postDtos,HttpStatus.OK);
    }

    @GetMapping("/get_post/{postId}")
    public ResponseEntity<PostDto> getPostById(@PathVariable("postId") Integer postId){
        PostDto postDto = this.postService.getPostById(postId);
        return new ResponseEntity<>(postDto,HttpStatus.OK);
    }


    @DeleteMapping("/delete_post/{postId}")
    public ResponseEntity<ApiResponse> deletePost(@PathVariable("postId") Integer postId)
    {
        this.postService.deletePost(postId);
        ApiResponse apiResponse = new ApiResponse("Deleted Successfully",true);
        return new ResponseEntity<>(apiResponse,HttpStatus.NO_CONTENT);
    }


    @PutMapping("/update_post/{postId}")
    public ResponseEntity<PostDto> updatePost(@RequestBody PostDto postDto, @PathVariable("postId")Integer postId){
     PostDto updatedPost =   this.postService.updatePost(postDto,postId);
     return new ResponseEntity<>(updatedPost,HttpStatus.ACCEPTED);
    }
}
