package com.blogging.application.controller;

import com.blogging.application.dto.UserDto;
import com.blogging.application.entity.ApiResponse;
import com.blogging.application.service.UserService;
import com.blogging.application.service.impl.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {


    @Autowired
    private UserService userService;

    @Autowired
    KafkaController kafkaController;

    String topicName = "user";

    @PostMapping("/create_user")
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserDto userDto) {

        UserDto createdUser = this.userService.createUser(userDto);
        kafkaController.publishJson(topicName, String.valueOf(createdUser.getId()), createdUser);
        return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
    }


    @GetMapping("/get_user/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable("id") int id) {
        UserDto user = this.userService.getUserById(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/list_users")
    public ResponseEntity<List<UserDto>> listUsers() {
        List<UserDto> userDtos = this.userService.getAllUsers();
        return new ResponseEntity<>(userDtos, HttpStatus.OK);
    }


    @PutMapping("/update_user/{id}")
    public ResponseEntity<UserDto> updateUser(@Valid @RequestBody UserDto userDto, @PathVariable("id") int id) {
        UserDto updatedUser = userService.updateUser(userDto, id);
        return new ResponseEntity<>(updatedUser, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("delete_user/{id}")
    public ResponseEntity<ApiResponse> deleteUser(@PathVariable("id") int id) {
        this.userService.deleteUserById(id);
        ApiResponse apiResponse = new ApiResponse("Application User Successfully Deleted", false);
        return new ResponseEntity<>(apiResponse, HttpStatus.NO_CONTENT);
    }


}
