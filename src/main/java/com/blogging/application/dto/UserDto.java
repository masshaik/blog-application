package com.blogging.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import validator.UserAlreadyExists;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private int id;
    @NotBlank(message = "Name should not be empty")
    private String name;
    @Email(message = "Email should be valid")
    @UserAlreadyExists
    private String email;
    @NotBlank(message = "Password should not empty")
    private String password;
    @NotBlank(message = "about should not be emtpy")
    private String about;

}
