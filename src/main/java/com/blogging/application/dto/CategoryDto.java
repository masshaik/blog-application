package com.blogging.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import validator.CategoryExistsConstraint;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDto {

    private int id;
    @NotBlank(message = "Category Name should not be empty")
    @CategoryExistsConstraint
    private String categoryName;
    @NotBlank(message = "Type should be mentioned")
    private String categoryType;
    @NotBlank(message = "Description should not be empty")
    private String description;

}
