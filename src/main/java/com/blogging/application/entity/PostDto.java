package com.blogging.application.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {

    private Integer id;

    private String title;
    private String content;

    private String imageName;

    private Date createdDate;

    private Category category;

    private User user;


}
