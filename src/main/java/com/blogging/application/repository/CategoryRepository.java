package com.blogging.application.repository;

import com.blogging.application.entity.Category;
import com.blogging.application.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Optional<Category>   findByCategoryName(String categoryName);

}
