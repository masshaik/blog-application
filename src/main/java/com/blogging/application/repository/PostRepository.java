package com.blogging.application.repository;

import com.blogging.application.entity.Category;
import com.blogging.application.entity.Post;
import com.blogging.application.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Integer> {
    List<Post> findByCategory(Category category);

    List<Post> findByUser(User user);
}
