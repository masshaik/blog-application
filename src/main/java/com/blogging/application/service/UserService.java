package com.blogging.application.service;

import com.blogging.application.dto.UserDto;
import com.blogging.application.entity.User;

import java.util.List;

public interface UserService {

        UserDto createUser(UserDto userDto);

        UserDto updateUser(UserDto userDto,Integer userId);

        UserDto getUserById(Integer userId);

        List<UserDto> getAllUsers();

        void deleteUserById(Integer userId);
}
