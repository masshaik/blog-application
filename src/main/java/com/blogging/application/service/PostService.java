package com.blogging.application.service;

import com.blogging.application.entity.Post;
import com.blogging.application.entity.PostDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PostService {

    Post createPost(PostDto postDto, int userId, int categoryId);

    PostDto updatePost(PostDto postDto,Integer postId);

    void deletePost(Integer postId);


    List<PostDto> getAllPost();

    PostDto getPostById(Integer postId);


    List<PostDto> getAllPostsByUser(Integer userId);

    List<PostDto> getAllPostsByCategory(Integer categoryId);


}
