package com.blogging.application.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducer {


    @Autowired
    KafkaTemplate<String, Object> kafkaTemplate;


    public void sendMessage(String topic ,String message) {
        kafkaTemplate.send(topic, message);
    }
}
