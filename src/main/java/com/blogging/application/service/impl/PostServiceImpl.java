package com.blogging.application.service.impl;

import com.blogging.application.entity.Category;
import com.blogging.application.entity.Post;
import com.blogging.application.entity.PostDto;
import com.blogging.application.entity.User;
import com.blogging.application.exceptions.ResourceNotFoundException;
import com.blogging.application.repository.CategoryRepository;
import com.blogging.application.repository.PostRepository;
import com.blogging.application.repository.UserRepository;
import com.blogging.application.service.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {


    @Autowired
    PostRepository postRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public Post createPost(PostDto postDto, int userId, int categoryId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User Not Found"));
        Category category = categoryRepository.findById(categoryId).orElseThrow(() -> new ResourceNotFoundException("Category Not Found"));


        Post post = modelMapper.map(postDto, Post.class);
        post.setImageName("default.png");
        post.setCreatedDate(new Date());
        post.setCategory(category);
        post.setUser(user);

        Post savedPost = postRepository.save(post);
        return savedPost;
    }

    @Override
    public PostDto updatePost(PostDto postDto, Integer postId) {

        Post post = this.postRepository.findById(postId).orElseThrow(() -> new ResourceNotFoundException("Post Does Not Exist"));
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        post.setImageName(postDto.getImageName());
        post.setCreatedDate(new Date());
        Post updatedPost = this.postRepository.save(post);

        return this.modelMapper.map(updatedPost,PostDto.class);
    }

    @Override
    public void deletePost(Integer postId) {
        this.postRepository.findById(postId).orElseThrow(() -> new ResourceNotFoundException("Post Does Not Exist"));
        this.postRepository.deleteById(postId);
    }

    @Override
    public List<PostDto> getAllPost() {
        List<Post> post = this.postRepository.findAll();
        List<PostDto> postDtos = this.postToPostDtos(post);
        return postDtos;
    }

    @Override
    public PostDto getPostById(Integer postId) {
        Post post = this.postRepository.findById(postId).orElseThrow(() -> new ResourceNotFoundException("Post Does Not Exist"));
        PostDto postDto = modelMapper.map(post, PostDto.class);
        return postDto;
    }

    @Override
    public List<PostDto> getAllPostsByUser(Integer userId) {

        User user = this.userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User Not Found"));
        List<Post> postList = this.postRepository.findByUser(user);
        List<PostDto> postDtos = postToPostDtos(postList);

        return postDtos;
    }

    @Override
    public List<PostDto> getAllPostsByCategory(Integer categoryId) {
        Category category = this.categoryRepository.findById(categoryId).orElseThrow(() -> new ResourceNotFoundException("Category  Not Found"));
        List<Post> byCategory = this.postRepository.findByCategory(category);
        List<PostDto> postDtos = postToPostDtos(byCategory);
        return postDtos;
    }

    private List<PostDto> postToPostDtos(List<Post> posts) {
        return posts.stream().map((post) -> this.modelMapper.map(post, PostDto.class)).collect(Collectors.toList());
    }
}
