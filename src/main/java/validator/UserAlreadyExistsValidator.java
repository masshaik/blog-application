package validator;

import com.blogging.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class UserAlreadyExistsValidator implements ConstraintValidator<UserAlreadyExists, String> {

    @Autowired
    UserRepository userRepository;

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {

        if (userRepository.findByEmail(email).isPresent()) {
            return false;
        }

        return true;
    }
}
