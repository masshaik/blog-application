package validator;

import com.blogging.application.entity.Category;
import com.blogging.application.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


class CategoryExistsConstriantValidator implements ConstraintValidator<CategoryExistsConstraint, String>
{

    @Autowired
    private CategoryRepository categoryRepository;

    public boolean isValid(String categoryName, ConstraintValidatorContext cxt) {

      Optional<Category> categoryOptional =  categoryRepository.findByCategoryName(categoryName);
      if(categoryOptional.isPresent()){
          return false;
      }
      return true;
    }
}






